package tirepressuremonitoringsystem;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import static helpers.AlarmBuilder.anAlarm;

public class AlarmTest {
    @Test
    public void testLowPressure() {
        Alarm alarm = anAlarm()
            .usingSensor(thatProbes(0.0))
            .withSafetyRange(17, 21)
            .build();
        alarm.check();
        assertTrue("Alarm should be on", alarm.isAlarmOn());
    }

    @Test
    public void HighPressureTest() {
        Alarm alarm = anAlarm()
            .usingSensor(thatProbes(30.0))
            .withSafetyRange(17, 21)
            .build();
        alarm.check();
        assertTrue("Alarm should be on", alarm.isAlarmOn());
    }

    @Test
    public void normalPressureTest() {
        Alarm alarm = anAlarm()
            .usingSensor(thatProbes(18.0))
            .withSafetyRange(17, 21)
            .build();
        alarm.check();
        assertFalse("Alarm sould be off", alarm.isAlarmOn());
    }

    @Test
    public void alarmStaysOnAfterAlert() {
        Alarm alarm = anAlarm()
            .usingSensor(thatProbes(0.0, 18.0))
            .withSafetyRange(17, 21)
            .build();
        alarm.check();
        assertTrue("Alarm should be on", alarm.isAlarmOn());
        alarm.check();
        assertTrue("Alarm should still be on", alarm.isAlarmOn());
    }

    private Sensor thatProbes(double value) {
        Sensor sensor = mock(Sensor.class);
        when(sensor.probeValue()).thenReturn(value);
        return sensor;
    }

    private Sensor thatProbes(double value1, double value2) {
        Sensor sensor = mock(Sensor.class);
        when(sensor.probeValue()).thenReturn(value1).thenReturn(value2);
        return sensor;
    }
}
