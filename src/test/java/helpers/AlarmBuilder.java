package helpers;

import tirepressuremonitoringsystem.Alarm;
import tirepressuremonitoringsystem.SafetyRange;
import tirepressuremonitoringsystem.Sensor;

public class AlarmBuilder {
    private Sensor sensor;
    private SafetyRange safetyRange;

    public static AlarmBuilder anAlarm() {
        return new AlarmBuilder();
    }

    public AlarmBuilder usingSensor(Sensor sensor) {
        this.sensor = sensor;
        return this;
    }

    public AlarmBuilder withSafetyRange(double lowThreshold, double highThreshold) {
        this.safetyRange = new SafetyRange(lowThreshold, highThreshold);
        return this;
    }

    public Alarm build() {
        return new Alarm(this.sensor, this.safetyRange);
    }
}