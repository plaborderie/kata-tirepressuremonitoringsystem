package tirepressuremonitoringsystem;

public class SafetyRange {
    private double lowThreshold;
    private double highThreshold;

    public SafetyRange(double lowThreshold, double highThreshold) {
        this.lowThreshold = lowThreshold;
        this.highThreshold = highThreshold;
    }

    public boolean contains(double pressureValue) {
        return pressureValue >= lowThreshold && pressureValue <= highThreshold;
    }

    public double getLowThreshold() {
        return lowThreshold;
    }

    public void setLowThreshold(double lowThreshold) {
        this.lowThreshold = lowThreshold;
    }

    public double getHighThreshold() {
        return highThreshold;
    }

    public void setHighThreshold(double highThreshold) {
        this.highThreshold = highThreshold;
    }
}