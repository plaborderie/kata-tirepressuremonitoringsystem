package tirepressuremonitoringsystem;

public interface Sensor {
    public double probeValue();
}