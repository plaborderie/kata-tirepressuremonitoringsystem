package tirepressuremonitoringsystem;

public class Alarm {
    Sensor sensor;
    boolean alarmOn;
    SafetyRange safetyRange;
    
    public Alarm(Sensor sensor, SafetyRange safetyRange) {
        this.sensor = sensor;
        this.safetyRange = safetyRange;
        alarmOn = false;
    }

    public void check() {
        double value = probeValue();

        if (isNotSafe(value)) {
            activateAlarm();
        }
    }

    private boolean isNotSafe(double value) {
        return !safetyRange.contains(value);
    }

    private double probeValue() {
        return sensor.probeValue();
    }

    private void activateAlarm() {
        alarmOn = true;
    }

    public boolean isAlarmOn() {
        return alarmOn; 
    }
}
